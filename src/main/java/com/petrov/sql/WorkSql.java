package com.petrov.sql;

import com.petrov.model.User;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by admin on 15.05.2018.
 */
@Component
public class WorkSql {

    private static Connection connection;
    private static final Logger LOG = LoggerFactory.getLogger(WorkSql.class);

    static {

        String url = null;
        String name = null;
        String password = null;

        try(InputStream in = WorkSql.class.getClassLoader().getResourceAsStream("persistence.properties")) {

            Properties properties = new Properties();
            properties.load(in);
            url = properties.getProperty("url");
            name = properties.getProperty("username");
            password = properties.getProperty("password");
        } catch (IOException e) {
            LOG.error("Error connecting to database!");
        }

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url,name,password);
        } catch (SQLException | ClassNotFoundException e) {
            LOG.error("Driver not found");
        }
    }
    public List<User> getAll() throws SQLException {

        List<User> users = new ArrayList<>();
        PreparedStatement ps = connection.prepareCall("SELECT * FROM users");
        ResultSet set = ps.executeQuery();

        while(set.next()) {

            User user = new User();
            user.setFirstName(set.getString(1));
            user.setLastName(set.getString(2));
            user.setOther(set.getString(3));
            users.add(user);
        }

        return users;
    }
}
