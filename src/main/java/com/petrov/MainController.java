package com.petrov;

import com.petrov.sql.WorkSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * Created by 777 on 14.05.2018.
 */
@Controller
public class MainController {

    private final Logger LOG = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private WorkSql workSql;

    @GetMapping("/training/hello")
    public String view() {
        LOG.info("Login successful");
        return "index";
    }

    @GetMapping("/training/users")
    public String getUsers(Model model) throws SQLException {
        model.addAttribute("users", workSql.getAll());
        LOG.info("Login in users successful");
        return "users";
    }
}
